#include <iostream>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <cmath>
#include <cstdlib>
#include <thread>
#include <functional>
#include <utility>
#include <mutex>
#include <algorithm>
#include <random>

#include "sdf.h"

int window_width = 800;
int window_height = 600;

float aspect_ratio = (float)window_width / (float)window_height;

void set_pixel(SDL_Surface* surface, int x, int y, glm::vec3 pixel) {
    
    Uint8 r = (Uint8)(glm::clamp(pixel.r, 0.0f, 1.0f)* 255);
    Uint8 g = (Uint8)(glm::clamp(pixel.g, 0.0f, 1.0f)* 255); 
    Uint8 b = (Uint8)(glm::clamp(pixel.b, 0.0f, 1.0f)* 255);

    Uint8* const target_pixel = ((Uint8*)surface->pixels
        + y * surface->pitch
        + x * surface->format->BytesPerPixel);

    target_pixel[0] = b;
    target_pixel[1] = g;
    target_pixel[2] = r;
}

float repeat(std::function<float(glm::vec3)> f, glm::vec3 p, glm::vec3 c) {
	glm::vec3 q = glm::mod(p + 0.5f * c, c) - 0.5f * c;
	return f(q);
}


float sdf_sphere(glm::vec3 point, glm::vec3 sphere_worldpos, float radius) {
    return glm::distance(point, sphere_worldpos) - radius;
}


float map(glm::vec3 point) {
	static glm::vec3 sphere_pos(0.0f, 0.0f, -1.5f);
    static float sphere_radius = 0.3f;
    constexpr SDF ball = sphere<sphere_pos, sphere_radius>();

	static glm::vec3 bigsphere_pos(-0.8f, 0.2f, -2.5f);
    static float bigsphere_radius = 0.5f;
    constexpr SDF bigball = sphere<bigsphere_pos, bigsphere_radius>();

	static glm::vec3 plane_normal(0.0f, 1.0f, 0.0f);
    static float plane_height = 0.3f;
    constexpr SDF floor = plane<plane_normal, plane_height>();

    SDF scene = unite<ball, floor, bigball>();

    return scene(point);

}

glm::vec3 gradient(glm::vec3 point) {
    float eps = 0.0005;
    glm::vec3 right(1.0f, 0.0f, 0.0f);
    glm::vec3 up(0.0f, 1.0f, 0.0f);
    glm::vec3 back(0.0f, 0.0f, 1.0f);

	float x = map(point + right * eps) - map(point - right * eps);
	float y = map(point + up * eps) - map(point - up * eps);
	float z = map(point + back * eps) - map(point - back * eps);

    return glm::normalize(glm::vec3(x, y, z));
}

float cdot(glm::vec3 a, glm::vec3 b) {
    return glm::clamp(glm::dot(a, b), 0.0f, 1.0f);
}

glm::vec4 phong(glm::vec3 normal, glm::vec3 lightdir, glm::vec3 eye, glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular, float specular_exponent) {
    float spec = glm::pow(cdot(normal, -glm::reflect(lightdir, normal)), specular_exponent);
    glm::vec3 color = ambient+ (cdot(normal, lightdir) * diffuse) + spec * specular;
    return glm::vec4(color, 1.0f);
}

float nrand() {
    return (float)rand() / (float)RAND_MAX;
}

float nnrand() {
    return 2.0f * (float)rand() / (float)RAND_MAX - 1.0f;
}

glm::vec3 random_in_hemisphere(glm::vec3 normal) {
    //stupid hack
    //create random normal
    auto v = glm::normalize(
        glm::vec3(
			nnrand(),
			nnrand(),
			nnrand()
		)
    );

    //if bad, flip it
	return glm::dot(v, normal) > 0.0f ?
		v : -v;
}

glm::vec3 random_reflected_in_hemisphere(glm::vec3 normal, glm::vec3 eye, int exponent) {
    auto new_normal = random_in_hemisphere(normal);
    while(exponent--)
        new_normal = -glm::reflect(eye, normal) + new_normal / 2.0f;
    return new_normal;
}


glm::vec4 trace(glm::vec3 origin, glm::vec3 direction, float distance, int samples, int iteration);
glm::vec4 divergent(glm::vec3 direction);

glm::vec4 material(glm::vec3 point, glm::vec3 normal, glm::vec3 eye, int iteration) {
    glm::vec3 ambient(0.1f, 0.1f, 0.0f);
    glm::vec3 diffuse(0.3f, 0.3f, 0.4f);
    glm::vec3 specular(1.0f, 1.0f, 1.0f);
    //return phong(normal, glm::normalize(glm::vec3(0.3f, 1.0f, 0.0f)), eye, ambient, diffuse, specular, 32);
    float epsilon = 0.01f;
	auto new_normal = random_reflected_in_hemisphere(normal, eye,  1);
//	auto new_normal = random_in_hemisphere(normal);
    auto trace_origin = point + epsilon * normal;
	if (iteration == 0) {
		return glm::vec4(0.0);
	} else {
		return 0.7f * trace(trace_origin, new_normal, 5.0, 50, iteration - 1);
	}
}

glm::vec4 retrace(glm::vec3 origin, glm::vec3 direction, float step, int iteration) {
    auto samplepoint = origin;
    int max_steps = 100;
    int steps = 0;
	while (map(samplepoint) > 0.0f) {
		samplepoint += step * direction;
        steps++;
        if (steps > max_steps)
            break;
	}
	return material(samplepoint, gradient(samplepoint), glm::normalize(-direction), iteration);
}

glm::vec4 divergent(glm::vec3 direction) {
    glm::vec3 sky_sun_color(1.0f, 1.0f, 0.8f);
    glm::vec3 sky_top_color(0.4f, 0.4f, 0.4f);
    glm::vec3 sky_bot_color(0.01f, 0.0f, 0.02f);
    float upness = (direction.y + 1.0f) / 2.0f;
    if (upness > 0.9f)
        return glm::vec4(sky_sun_color, 0.0f);
    else
		return glm::vec4(glm::mix(sky_bot_color, sky_top_color, upness), 0.0f);
}

glm::vec4 trace(glm::vec3 origin, glm::vec3 direction, float distance, int samples, int iteration) {
    glm::vec3 samplepoint = origin;
    glm::vec3 previous = origin;
	float step = distance / (float)samples;

	for (int i = 0; i < samples; i++) {
        float value = map(samplepoint);
		if (value <= 0.0f) {
            int retrace_samples = 50;
			auto newstep = step / (float)retrace_samples;
            return glm::vec4(glm::vec3(retrace(previous, direction, newstep, iteration)), 1.0f);
		}
        float advance = glm::max(step, value);
        previous = samplepoint;
		samplepoint = samplepoint + direction * advance;
	}
    return divergent(direction);
}


glm::vec4 surface_point(int x, int y) {
    float xpos = (float)x / (float)window_width - 0.5f;
    float ypos = -((float)y / (float)window_height -0.5f) / aspect_ratio ;
    glm::vec3 viewplane_worldpos(xpos, ypos, 0.0f);
    glm::vec3 eye_worldpos(0.0f, 0.0f, 1.0f);

    glm::vec3 direction = glm::normalize(viewplane_worldpos - eye_worldpos);

    int samples = 120;
    float length = 8.0f;
    int iterations = 4;

    int rays = 10;
    glm::vec4 accum_color(0.0f);
    for (int i = 0; i < rays; i++) {
        auto color = trace(eye_worldpos, direction, length, samples, iterations);
        //use a as "has hit geometry"
        if (color.a == 0.0f) {
            return color;
        }
		accum_color +=  color / (float) rays;
    }
    return accum_color;
}

void render(SDL_Surface* surface, int x, int y, int width, int height) {
    for(int i = 0; i < width; i++)
        for (int j = 0; j < height; j++) {
            set_pixel(surface, x+i, y+j, surface_point(x+i, y+j));
        }
}

std::vector<std::pair<int, int>> workpieces;
std::mutex mutex;

void render_chunks(SDL_Surface* surface, int width, int height) {
	for (;;) {
		mutex.lock();

		if (workpieces.empty()) { 
			mutex.unlock();
			return;
		}
		std::pair<int, int> p = workpieces.back();
		workpieces.pop_back();
		mutex.unlock();

		int x = p.first;
		int y = p.second;
        render(surface, x, y, width, height);
	}
}

int main(int argc, char **argv) {
    srand(5);
    SDL_Init(0);
    SDL_Window* window = SDL_CreateWindow("path tracer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, 0);
	SDL_Surface* window_surface = SDL_GetWindowSurface(window);

    std::cout << aspect_ratio << std::endl;

    int horizontal_chunks = 100;
    int vertical_chunks = 100;
	int chunk_width = window_width / horizontal_chunks;
	int chunk_height = window_height / vertical_chunks;
	for (int i = 0; i < horizontal_chunks; i++) {
        for (int j = 0; j < vertical_chunks; j++) {
            workpieces.push_back(std::pair<int, int>(i*chunk_width, j*chunk_height));
		}
	}
    auto rng = std::default_random_engine{};
    std::shuffle(workpieces.begin(), workpieces.end(), rng);

    int number_threads = 6;

    for(int i =0; i<number_threads; i++)
			new std::thread(render_chunks, window_surface,  chunk_width, chunk_height);


    bool quit = false;
    SDL_Event event;


    while (quit == false) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
        }
        SDL_UpdateWindowSurface(window);
        SDL_Delay(16);
    }
    SDL_SaveBMP(window_surface, "img.bmp");

    return 0;
}
