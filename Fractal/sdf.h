#pragma once
#include<functional>
#include<glm/glm.hpp>
#include<glm/ext.hpp>

using SDF = float(*)(glm::vec3);

template<SDF a, SDF b, SDF... tail>
requires (sizeof...(tail) == 0)
constexpr SDF unite() {
	return [](glm::vec3 point) {return glm::min(a(point), b(point)); };
}

template<SDF a, SDF... tail>
constexpr SDF unite() {
	return unite<a, unite<tail...>()>();
}

template<SDF a, SDF b>
constexpr SDF intersect() {
	return [](glm::vec3 point) {return glm::max(a(point), b(point)); };
}

template<SDF a, SDF b>
constexpr SDF cut() {
	return [](glm::vec3 point) {return glm::max(a(point), -b(point)); };
}

template<SDF a, glm::vec3& period>
constexpr SDF repeat() {
	return [](glm::vec3 point) {
		glm::vec3 q = glm::mod(point + 0.5f * period, period) - 0.5f * period;
		return a(q);
	};
}

template<SDF a, glm::vec3& v, float& k>
constexpr SDF twist() {
	return [](glm::vec3 p) {
		float c = cos(k * p.y);
		float s = sin(k * p.y);
		glm::mat2  m = glm::mat2(c, -s, s, c);
		glm::vec3  q = glm::vec3(m * glm::vec2(p.x, p.z), p.y);
		return a(q);
	};
}

template<glm::vec3& pos, float& radius>
constexpr SDF sphere() {
	return [] (glm::vec3 point){return glm::distance(point, pos) - radius; };
}

template<glm::vec3& pos, glm::vec3& dimensions>
constexpr SDF box() {
	return [](glm::vec3 point) {
		point = point - pos;
		auto q = glm::abs(point) - dimensions;
		return glm::length( glm::max(q, 0.0f)) + glm::min(0.0f, glm::max(q.x, glm::max( q.y, q.z ) ) );
	};
}

template<SDF a, glm::mat4& invmat>
constexpr SDF transform() {
	return [](glm::vec3 point) {
		return a(invmat * glm::vec4(point, 1.0f));
	};
}


template<glm::vec3& n, float& h>
constexpr SDF plane() {
	return [](glm::vec3 point) {
		return glm::dot(point, n) + h;
	};
}
